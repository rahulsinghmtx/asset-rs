/*
* Name		: GetDependentPicklistValue
* Created By: Rahul Singh 01/06/20
* Purpose	: Utilized to fetch Map of controlling picklist value with list of dependent picklist.
*
* Date         New/Modified      Author             Description 
*
*/ 
public class GetDependentPicklistValue {
    
    /*  
    @Name           : fetchDependentPicklistlist
    @Description    : fetch Map of controlling picklist value with list of dependent picklist as label & value by passing dependent Field Api Name [I.e Contact.Type__c]
    @Params         : Schema.sObjectField
    @Return			: Map<String, List<DependentWrapper>>
    */ 
    public static Map<String, List<DependentWrapper>> fetchDependentPicklistlist(Schema.sObjectField dependentObjectAndFieldApi) {
        Schema.DescribeFieldResult depend = dependentObjectAndFieldApi.getDescribe();
        Schema.sObjectField controlToken = depend.getController();
        if (controlToken == null) { 
            return new Map<String, List<DependentWrapper>>();
        } 
        
        Schema.DescribeFieldResult control = controlToken.getDescribe();
        List<Schema.PicklistEntry> controlEntries;
        if(control.getType() != Schema.DisplayType.Boolean) {
            controlEntries = control.getPicklistValues();
        }
        String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        Map<String,List<DependentWrapper>> dependentPicklistValues = new Map<String,List<DependentWrapper>>();
        for (Schema.PicklistEntry entry : depend.getPicklistValues()) {
            if (entry.isActive() && String.isNotEmpty(String.valueOf(((Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')))) {
                List<String> base64chars =
                    String.valueOf(((Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')).split('');
                for (Integer index = 0; index < (controlEntries != null ? controlEntries.size() : 2); index++) {
                    Object controlValue =
                        (controlEntries == null
                         ?   (Object) (index == 1)
                         :   (Object) (controlEntries[index].isActive() ? controlEntries[index].getLabel() : null)
                        );
                    Integer bitIndex = index / 6;
                    if (bitIndex > base64chars.size() - 1) {
                        break;
                    }
                    Integer bitShift = 5 - Math.mod(index, 6);
                    if  (controlValue == null || (base64map.indexOf( base64chars[ bitIndex ] ) & (1 << bitShift)) == 0)
                        continue;
                    if (!dependentPicklistValues.containsKey((String) controlValue)) {
                        dependentPicklistValues.put((String) controlValue, new List<DependentWrapper>());
                    }
                    dependentPicklistValues.get((String) controlValue).add(new DependentWrapper(entry.getLabel(),entry.getValue()));
                }
            }
        } 
        
        for(string stg: dependentPicklistValues.keyset()){
            system.debug('[GetDependentPicklistValue.fetchDependentPicklistlist] :'+stg +' :'+dependentPicklistValues.get(stg).size());
        }
        System.debug('[GetDependentPicklistValue.fetchDependentPicklistlist] dependentPicklistValues :'+ dependentPicklistValues);
        return dependentPicklistValues;
    } 

    public class DependentWrapper{
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public String value {get;set;}
        public DependentWrapper(String label,String value){
            this.label = label;
            this.value = value;
        }
    }
}