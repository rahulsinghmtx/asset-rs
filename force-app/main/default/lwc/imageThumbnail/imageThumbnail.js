import { LightningElement,api,wire,track } from 'lwc';
import generateImageUrl from '@salesforce/apex/ImageThumbnailController.generateImageUrl';

export default class ImageThumbnail extends LightningElement {
    
    //------------- Configure Section -------------//
    @track imageStyle =`height: 100px; width:100px`;
    @track generateURLFromParent = false;  
    //------------- Configure Section -------------//

    
    @api url;       // url can be passed from parent component
    @api altText;   // altText can be passed from parent component
    error; 
    stack;
    
    errorCallback(error, stack) {
        this.error = error;
        console.log("ImageThumbnail ErrorCallback", JSON.stringify(error));
    }

    connectedCallback(){
        if(this.generateURLFromParent === false){
            this.generateImage();
        }
    }  
     
    /*Use below method to generate Image URL*/ 
    generateImage(){
        generateImageUrl({contentversionId :'0682g000000dDP8AAM'})
        .then(result=>{
            //console.log("[ImageThumbnail.generateImage] Result :", JSON.stringify(result));
            if(result !=null){
                this.url = result;
            }else{
                console.log("[ImageThumbnail.generateImage] Image url not found ");
            }
        })
        .catch(error=>{
            console.log("[ImageThumbnail.generateImage] Error :", JSON.stringify(error));
        })
    }

}