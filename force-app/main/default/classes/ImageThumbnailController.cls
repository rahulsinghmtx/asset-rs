public with sharing class ImageThumbnailController {

    /* Configure Section */
    Public static String orgDocumentUrlBase = Label.x7S_ImageThumbnailBaseUrl;
    /* Configure Section */

    public ImageThumbnailController() {} 

     /*  
    @Name           : generateImageUrl
    @Description    : generate image thumbnail url.
    @Params         : content version record Id
    @Return			: string
    */
    @AuraEnabled 
    public static String generateImageUrl(String contentversionId){
        String imageUrl;
        List<contentversion> contentversionList = [SELECT Id, Title,CreatedDate,Active__c, 
                                                    ContentDocumentId, ContentBodyId
                                                    FROM ContentVersion
                                                    WHERE Id =:contentversionId Limit 1];
        if(! contentversionList.isEmpty()){ 
            imageUrl = orgDocumentUrlBase+
                  'sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId='+
                  contentversionList[0].Id+
                  '&operationContext=CHATTER&contentId='+
                  contentversionList[0].ContentBodyId;
        } 
        system.debug('[ImageThumbnailController.generateImageUrl] :'+imageUrl);
        return imageUrl;
    } 


}
