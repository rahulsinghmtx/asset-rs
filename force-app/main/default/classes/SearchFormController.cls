public class SearchFormController {
   
    Public static string query = 'SELECT Id,Name,Phone, Email FROM Contact WHERE ('; // [ATR] [According To Requiremnt]
    //Public static string query = 'SELECT Id, Name, Quantity__c FROM SFDC_Purchase_Requisition__c WHERE ('; // [ATR] [According To Requiremnt]
    
    /*
    * Method Name: searchFormList
    * Description: Search based on form keyword.
    * @param: String paramsString
    * @return: Sobject List
    */
    
    @AuraEnabled
    Public static List<Sobject> searchFormList(String paramsString){
        system.debug('paramsString >>>'+paramsString);
        
        List<ParamWrapper> paramWrappers = (List<ParamWrapper>) JSON.deserializeStrict(paramsString, List<ParamWrapper>.class);
        List<Sobject> sobjectRecordList = new List<Sobject>();
        List<string> paramsConditionList = new List<string>();
        for(ParamWrapper parWrap: paramWrappers){
            if(String.isNotEmpty(parWrap.value)){
                String temp = parWrap.value;
                String replaceString =  '\\'+'\'';
                if(temp.contains('\'')){
                    temp = parWrap.value.replace('\'', replaceString);
                }
                paramsConditionList.add(parWrap.apiName+ ' LIKE \''+temp+ '%'+'\'');
                //paramsConditionList.add(parWrap.apiName+ ' LIKE \'%'+parWrap.value+ '%'+'\'');
            }	
        } 
        query += getWhereClause(paramsConditionList);
        query += ')';
        for(Sobject conRecd : database.query(query)){
            sobjectRecordList.add(conRecd);
        }
        system.debug('[SearchFormController.searchFormList] sobjectRecordList :'+sobjectRecordList);
        return sobjectRecordList;
    } 
    
    private static String getWhereClause(List<String> conditions) {
        return String.join(conditions, ' AND ');
    }
    
    public class ParamWrapper {
        @AuraEnabled public String apiName;
        @AuraEnabled public String value;
    } 
}