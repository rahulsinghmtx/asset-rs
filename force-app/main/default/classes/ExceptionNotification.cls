/*
* Name		: ExceptionNotification 
* Created By: Rahul Singh 22/08/2020
* Purpose	: Utilized to send method exception notfication to users.
*
* Date         New/Modified      Author             Description 
* 01/09/2020   Modified  		 Rahul.S			Added Org Base Url. 
*/ 

public class ExceptionNotification {

    /*  
    @Name           : sendExceptionNotification
    @Description    : Send method exception notfication to users
    @Params         : Strings
    @Return         : null
	@Info			: Param [payload- can be used to get intgeration request or any other additional information.]
					: Email deliverability must be All Email.
    */
    public static void sendExceptionNotification(String classAndMethodName, Exception e, String Id, String payload) {
        
        try {
            List<String> toRecipient = new List<String>();
            if(System.Label.ExceptionNotificationToRecipients != Null){
                toRecipient = System.Label.ExceptionNotificationToRecipients.split(',');
            }
            system.debug('[ExceptionNotification.sendExceptionNotification] toRecipient :'+toRecipient.size());
            
            if(! toRecipient.isEmpty())
            {
              	String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setHtmlBody(
                        '<h3>Error</h3>' +
                                '<p>Organization Id And Name: ' + System.UserInfo.getOrganizationId() + ' ' + System.UserInfo.getOrganizationName() + '</p>' +//Added this by SS: to know about the Organization for NCM-2560
                                '<p>Org Base Url: ' + sfdcBaseURL + '</p>' +
                                '<p>Produced By: ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + '(' + UserInfo.getUserEmail() + ')</p>' +
                                '<p>Class And Method Name: ' + classAndMethodName + '</p>' +
                                '<p>Id: ' + Id + '</p>' +
                                '<p>Message: ' + e.getMessage() + ': ' + e.getStackTraceString() + '</p>' +
                                '<p>Payload: ' + payload + '</p>'
                );
                email.setSubject('Error occured [' + DateTime.now().format() + ']');
                email.setToAddresses(toRecipient);
    
                if (!Test.isRunningTest()){
                    Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email});
                }  
            }
        }catch(Exception ce) {
            System.debug('[ExceptionNotification.sendExceptionNotification] Exception Occured :'+ce.getCause() +' Line No :' +ce.getLineNumber() + ce.getMessage());
        }  
    }
}