({
    doInit : function(component, event, helper) {
       //console.log('Participant Search doInit :');
     },
    
    initiateSearch : function(component, event, helper) {
        component.set("v.tableSpinner", true);
        helper.initiateSearchHelper(component, event, helper);
    },
    
    handleSort : function(component,event,helper){
        var sortBy = event.getParam("fieldName");
        var sortDirection = event.getParam("sortDirection");
        component.set("v.sortBy",sortBy);
        component.set("v.sortDirection",sortDirection);
        var data = component.get("v.searchRecordList");
        var sortedData = helper.datatableSorting(component,event,helper,sortBy,sortDirection,data);
        //set sorted data to accountData attribute
        component.set("v.searchRecordList",sortedData);
    },
    
    viewRecord : function(component,event,helper){
        var viewRecordNavigationUrl = component.get("v.viewRecordNavigationUrl");
        if(viewRecordNavigationUrl !='')
        {
            var recId = event.getParam('row').Id;
            var actionName = event.getParam('action').name;
            var urlNav = viewRecordNavigationUrl+'/'+recId;
            if (actionName == 'View' && ((!$A.util.isUndefinedOrNull(recId)) || (!$A.util.isEmpty(recId)))) {
                //helper.gotoNavigationWithParam(urlNav); 	// Open in same window
                window.open(urlNav, '_blank'); 		// Open in new window.
            }
        }else{ 
            helper.showMessage("Error :",'Navigation URL attribute is empty.',"error");
        }
        
    },
    
})