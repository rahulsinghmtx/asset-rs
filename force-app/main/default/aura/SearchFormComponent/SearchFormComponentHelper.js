({

initiateSearchHelper : function(component, event, helper) {
    var myInputs = component.find("searchForm").find({instancesOf : "lightning:input"});
    let fieldApiNameAndValue = new Map();
    var myParams = [];
    var checkFieldValue = 0;
    for(var i = 0; i < myInputs.length; i++){
        var key = myInputs[i].get("v.name");
        var value = myInputs[i].get("v.value");
        if(value == null || value == ''){
            checkFieldValue++;
        }
        var myParam = {
            'apiName' : key,
            'value' : value
        };
        myParams.push(myParam);
    }
    console.log('paramsString :',JSON.stringify(myParams));
    if(checkFieldValue == myInputs.length){
        component.set("v.tableSpinner", false);
        var Message = component.get("v.Error_Message_Enter_Search_Key");
        helper.showMessage("Error",Message,"error");
    }else{
         helper.doCallout(component,'c.searchFormList', {'paramsString' : JSON.stringify(myParams)})
    .then(
           $A.getCallback(result =>{
                if(result){
                	//console.log('getParticipantHelper result >>>'+JSON.stringify(result));
                    component.set("v.noRecordMessage", false);
                    component.set("v.tableSpinner", false);
                    component.set("v.searchRecordList", result);
                    var message = result.length +' Record Found' ;
                    helper.showMessage("Search: ",message,"info"); 
                    if(result.length == 0)
                    {
                        component.set("v.noRecordMessage", true);
                    }
                }
            }),
            $A.getCallback(error => {
                component.set("v.noRecordMessage", true);
                console.log('[SerchFormComponent] Error :',JSON.stringify(error));
                helper.showMessage("ERROR IN SERVER CALL",error,"error");
            })
        );
    }

},

})