/**
 * Created by ---- on 2019-06-07.
 */
({
    /*
     * This function displays toast based on the parameter values passed to it
     * */
    showToast : function(params) {
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            if(!params){
                toastEvent.setParams({
                    "title": "TOAST ERROR!",
                    "type": "error",
                    "message": "Toast Param not defined"
                });
                toastEvent.fire();
            } else{
                toastEvent.setParams(params);
                toastEvent.fire();
            }
        } else{
            alert(params.message);
        }
    },
    
    /*
    * @description :Use this method to call any apex class method
    * @params :  component : instance of component
    *         : method : name of apex class method (for ex. 'c.getAccount')
    *         : params : params needs to be passed to apex class. should be in json format. for example {'nameString' : 'testName'}
    * @return : promise with either results or error
    */
    doCallout: function (component, method, params) {
        console.log('doCallout params :',params);
        return new Promise($A.getCallback(function (resolve, reject) {
            // Set action and param
            var action = component.get(method);
            if (params != null) {
                action.setParams(params);
            }
            // Callback
            //
            action.setCallback(component, function (response) {
                console.log('in action');
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    resolve(response.getReturnValue());
                } else {
                    var errors = response.getError();
                    reject(errors);
                }
            });
            $A.enqueueAction(action);
        }));
    },
    /*
    * @description : Use this method to show any toast message
    * @params : title : title for toast
    *         : message : message to show in toast
    *         : type : type can be info , success,warning,error
    * @return : promise with either results or error
    */
    showMessage: function(title, message,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title" : title,
            "message" : message,
            "type" : type
        });
        toastEvent.fire();
    },
    /*
    * @description : Use this method to open any custom component in modal
    * @params : component : instance of component
    *         : componentName : name of custom component to open in modal
    *         : attributes : attributes of custom component in json format
    *         : customClasses : any custom classes to apply on modal
    * @return : promise with either results or error
    */
    openModal : function(component,componentName,attributes,customClasses){
        return new Promise(function(resolve,reject){
            var overlayLib = component.find('overlayLib');
            //if component has overlay library, then only go ahead otherwise show error
            if(componentName  && componentName.trim() !== ''){
                if(overlayLib){
                    $A.createComponent(
                        componentName,
                        attributes || {},
                        function(content, status, errorMessage){
                            if (status === "SUCCESS") {
                                component.find('overlayLib').showCustomModal({
                                    body : content || '',
                                    cssClass : customClasses || ''
                                }).then(function(modal){
                                    resolve(modal);
                                },function(error){
                                    reject();
                                    console.log('ERROR::MTXBASE::Error while creating overlay '+ error);
                                })
                            }else{ //error while creating new component
                                reject();
                                console.log('ERROR::MTXBASE::Error while creating new component '+ errorMessage);
                            }
                        }
                    );
                }else{
                    reject();
                    console.log('ERROR::MTXBASE::Please add "<lightning:overlayLibrary aura:id="overlayLib" />" in child component ');
                }
            }else{
                reject();
                console.log('ERROR::MTXBASE::Custom component name not provided ');
            }
        });
        
    },
    /*
    * @description : Use this method to check if required fields are valid or not
                   : use only for fields which doesnt have standard checkValidity for ex. lightning:inputField
                   : it will assign them a class that is passed as parameter if its not valid
    * @params : component : instance of component
    *         : errorClass : css class to assign if field is not valid
    *         : listOfFields : list of aura ids of fields to validate
    *
    * @return : returns true if even a single field is not valid
    */
    checkRequired : function(component,errorClass,listOfFields){
        let validSoFar = true;
        var _this = this;
        
        for(var x=0;x<listOfFields.length;x++){
            if(!_this.checkRequiredFieldById(component,errorClass,listOfFields[x]))
                validSoFar = false;
        }
        return validSoFar;
    },
    
    
    //check required on individual fields
    checkRequiredFieldById : function(component,errorClass,auraId){
        let _this = this;
        let field = component.find(auraId);
        if(field){
            if(field.constructor === Array){
                let validSoFar = true;
                for(var x=0;x<field.length;x++){
                    if(!_this.checkRequiredIndividualField(errorClass,field[x]))
                        validSoFar = false;
                }
                return validSoFar;
            }else{
                return _this.checkRequiredIndividualField(errorClass,field);
            }
        }else{
            return true;
        }
    },
    
    //check required on individual fields
    checkRequiredIndividualField : function(errorClass,field){
        let value = field.get("v.value");
        if(!value || value.trim().length == 0){
            $A.util.addClass(field, errorClass);
            return false;
        }else{
            $A.util.removeClass(field, errorClass);
            return true;
        }
    },
    goToDetailPage : function(recordId) {
        var sObectEvent = $A.get("e.force:navigateToSObject").setParams({
            "recordId": recordId,
            "slideDevName": "detail"
        }).fire();
        
    },
    navigateToURL : function(url) {
        //Find the text value of the component with aura:id set to "address"
        var address = '/'+url;
        let fullURL = window.location.href.split('?');
        if(!$A.util.isEmpty(fullURL) && fullURL.length > 1){
            address += "?"+fullURL[1];
        }
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": address,
            "isredirect" :false
        });
        urlEvent.fire();
        
    },
    /*datatableSorting : function(component,event,helper,fieldName,data,reverse,primer){
       console.log('new sorting way >>'); 
            var key = primer ? function(x) {return primer(x[field])} : function(x) {return x[field]};
            reverse = !reverse ? 1 : -1;
            return function (a, b) {
                return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
            }
        
    },*/
    datatableSorting : function(component,event,helper,fieldName,sortDirection,data,fieldType){
        console.log('fieldType >>',fieldType);
        //function to return the value stored in the field
        var key = function(a) { return a[fieldName]; }
        var reverse = sortDirection == 'asc' ? 1: -1;
        
        // to handel number/currency type fields 
        if(fieldName == 'NumberOfEmployees'){ 
            data.sort(function(a,b){
                var a = key(a) ? key(a) : '';
                var b = key(b) ? key(b) : '';
                return reverse * ((a>b) - (b>a));
            }); 
        }
        else{// to handel text type fields 
            data.sort(function(a,b){ 
                var a = key(a) ? key(a).toLowerCase() : '';//To handle null values , uppercase records during sorting
                var b = key(b) ? key(b).toLowerCase() : '';
                return reverse * ((a>b) - (b>a));
            });    
        }
        //return sorted data
        return data;
    },
    gotoNavigationWithParam: function(urlParam){
        //console.log('gotoNavigationWithParam Base >>',urlParam);
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": urlParam
        });
        urlEvent.fire();
	},
})