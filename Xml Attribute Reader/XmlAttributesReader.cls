/*
* Name		:XmlAttributesReader
* Created By: Rahul Singh 01/06/20
* Purpose	: Utilized to read xmlResponse and return tag attributes as map<string,string>.
*
* Date         New/Modified      Author             Description 
*
*/ 
public class XmlAttributesReader {
	
    public static set<string> childNodes;
    public static map<string,string> mapKeyValue;
	
    // Constructor
    public XmlAttributesReader(){
      mapKeyValue = new map<string,string>();
      childNodes = new set<string>{'ROW','ROOT'}; // Mention Tag to get attributes
    }
	
	/*  
    @Name           : parseFromString
    @Description    : used to parse xmlResponse in string to get attributes as map.
    @Params         : string(xmlResponse)
    @Return			: map<string,string>
    */ 
    public map<string,string> parseFromString(string xmlResponse) {
        system.debug('Class[XmlAttributesReader] parseFromString :'+xmlResponse !=null); 
        XmlStreamReader reader = new XmlStreamReader(xmlResponse);
        reader.setCoalescing(true);
        return parseXmlReader (root , reader);
    }
    
    integer countStartElement = 0 ;
    integer countEndElement = 0 ; 
    integer countwhile = 0 ;
    public map<string,string> parseXmlReader( Element parent, XmlStreamReader reader) {
        countwhile++;
        try {
            while(reader.hasNext()) {
                countStartElement++;
                if (reader.getEventType() == XmlTag.START_ELEMENT) {
                    Element child = new Element(reader.getLocalName()); 
                    //system.debug('@countStartElement: '+countStartElement + ' '+' @reader location:' + reader.getLocation() + ' @nodeName: ' +child.nodeName +' @nodeAttributeCount: '+reader.getAttributeCount());
                    for (integer i=0; i<reader.getAttributeCount(); i++) {
                        IF(childNodes.contains(child.nodeName)){
                            //system.debug('@countStartElement: '+countStartElement + ' '+' @reader location:' + reader.getLocation() + ' @nodeName: ' +child.nodeName +' @nodeAttributeCount: '+reader.getAttributeCount());
                            //system.debug('key: '+reader.getAttributeLocalName(i) +' ,value: '+reader.getAttributeValueAt(i));
                            mapKeyValue.put(reader.getAttributeLocalName(i),reader.getAttributeValueAt(i));
                        }
                        child.attributes.put(reader.getAttributeLocalName(i),reader.getAttributeValueAt(i));
                    }
                    if (reader.hasNext()) reader.next();
                    else return mapKeyValue;
                    parseXmlReader(child, reader) ; // recurse
                } else if (reader.getEventType() == XmlTag.END_ELEMENT) {
                    countEndElement++;
                    reader.next();
                    return mapKeyValue;
                } else if (reader.getEventType() == XmlTag.CHARACTERS) {
                    if ( ! reader.isWhitespace()) {
                        parent.nodeValue += reader.getText();
                        system.debug('Class[XmlAttributesReader] reader.getText()'+reader.getText());// add text to current element
                    }
                    reader.next();
                }
                else {
                    reader.next();
                }
            }
        } catch(Exception e) { // ParseError if we get a truncated response, allow it
            system.debug('Class[XmlAttributesReader] parseXmlReader Exception '+e);
            parent.dumpAll();
        }
        system.debug('Class[XmlAttributesReader] mapKeyValue :'+mapKeyValue);
        //system.debug('countStartElement >'+countStartElement); 
        //system.debug('countEndElement >'+countEndElement); 
        //system.debug('countwhile >'+countwhile); 
        return mapKeyValue;
    }
     
    
    // everything in the dom is found as childNodes under this root element
    public Element root = new Element('#document');
    public integer debug =0;
    public class Element {
        public Element(string n) {  
            nodeName = n; 
        }
        public void dumpAll() { 
            //system.debug('dumpAll blank>');
            dumpAll('');    
        } 
        public void dumpAll(string pre) { // node and all children
            system.debug( pre + this.nodeName + '->' + this.nodeValue + ' ' + this.attributes );
            for (Element c: this.childNodes) {
                c.dumpAll(pre+'   ');
            }
        }
        
        // properties
        public Element parentNode = null; // only root has a null parent
        public string nodeName = '';
        public string nodeValue = '';
        public List<Element> childNodes = new List<Element>();
        public map<String,String> attributes = new map<String,String>();
        
    }
}