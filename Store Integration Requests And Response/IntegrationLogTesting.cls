public class IntegrationLogTesting {
    
    public static void integrationLogTest(){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response  = new HttpResponse();
        
        try{
            request.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals');
            request.setMethod('GET');
            response = http.send(request);
            // If the request is successful, parse the JSON response.
            if (response.getStatusCode() == 200) {
                // Deserialize the JSON string into collections of primitive data types.
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                // Cast the values in the 'animals' key as a list
                List<Object> animals = (List<Object>) results.get('animals');
                System.debug('Received the following animals:');
                for (Object animal: animals) {
                    System.debug(animal);
                }
            }
            if (response != null) {
                system.debug('Inside Response :');
                CalloutIntegrationLog.createCalloutLog(request.getEndpoint(), request.getBody(), response.getBody(), response.getStatusCode());
            }
        }catch (Exception e) {
            System.debug('Class [x7S_CalloutHandler] performCallout Exception : '+e.getMessage());
            System.debug('Class [x7S_CalloutHandler] performCallout StackTrace: '+e.getStackTraceString());
            CalloutIntegrationLog.createExceptionLog(request.getEndpoint(), request.getBody(), e.getMessage(), e.getTypeName());   
        }finally{
            System.debug('Class [x7S_CalloutHandler] Insert TransactionLog : ');
            insertransactionLog();
        }
    }
    
    /*Create TransactionLog On Every Integration Call*/
    public static void insertransactionLog(){ 
        List<Transaction_Log__c> transactionLogInsertList = CalloutIntegrationLog.integrationLogList();
        try { 
            if(! transactionLogInsertList.isEmpty()){
                insert transactionLogInsertList;
            }
        }catch (Exception e){
            System.debug('Class [x7S_CACIntegrationHelper] insertransactionLog Exception :'+e.getCause() +' '+e.getLineNumber() +' '+ e.getMessage()+' '+e.getStackTraceString());
        }
    }
}