/*
* Class Name: CalloutIntegrationLog
* Description: Creates an Integration Log record to track callout errors.
* Author/Date:  Rahul Singh, 04/27/2020
*
* Date         New/Modified      Author             Description 
* 
*/
public with sharing class CalloutIntegrationLog {
    private static final String FAIL_STATUS = 'Failure';
    private static final String SUCCESS_STATUS = 'Success';
    private static final String PENDING_STATUS = 'Pending';
    private static final Integer MAX_LONG_LENGTH = 131072;
    private static final Integer MAX_Text_LENGTH = 255;
    public static list<Transaction_Log__c> transactionLogInsertList = new list<Transaction_Log__c>();
         
    /**
    * Creates an Integration Log record to track callout errors on anything that isn't HTTPStatusCode = 200
    * @param request  [HTTPRequest sent during callout]
    * @param response [HTTPResponse received from callout]
    */ 
    public static void createCalloutLog(String endpoint, String requestBody, String responseBody, Integer statusCode) {
        
        Transaction_Log__c log = new Transaction_Log__c(); 
        log.method__c = endpoint.left(MAX_Text_LENGTH);
        log.request_Url__c = endpoint;
        log.Direction__c = 'Outbound'; 
        log.User__c = UserInfo.getUserId();  
        log.Status_Message__c = 'Status Code: ' + statusCode;
        log.Request__c = requestBody.left(MAX_LONG_LENGTH);
        log.Response__c = responseBody.left(MAX_LONG_LENGTH);
        log.Status__c = statusCode == 200 ? SUCCESS_STATUS : FAIL_STATUS;
        if (log.Status__c == FAIL_STATUS) {
            log.Trigger_Email__c = true; 
        }
        insertLog(log);
    }
    
    /**
    * Creates an Integration Log record to track callout exceptions 
    * @param request  [HTTPRequest sent during callout]
    * @param e [CalloutException hit during execution]
    */
    public static void createExceptionLog(String endpoint, String requestBody, String exceptionMessage, String exceptionType) {
        Transaction_Log__c log = new Transaction_Log__c();
        log.method__c = endpoint.left(MAX_Text_LENGTH);
        log.request_Url__c = endpoint;
        log.Direction__c = 'Outbound';
        log.User__c = UserInfo.getUserId();  
        log.Status_Message__c = exceptionType;
        log.Request__c = requestBody.left(MAX_LONG_LENGTH);
        log.Response__c = exceptionMessage.left(MAX_LONG_LENGTH);
        log.Status__c = FAIL_STATUS; 
        insertLog(log);
    } 
    
    /**
    * Add Transaction Log in list
    */
    private static void insertLog(Transaction_Log__c log) {
         transactionLogInsertList.add(log);
    }
    
    /**
    * Return Transaction Log list.
    */ 
    public static list<Transaction_Log__c> integrationLogList(){
        system.debug('Class [CalloutIntegrationLog] return transactionLogInsertList*** '+transactionLogInsertList);
        return transactionLogInsertList;
    }
}